use std::env;

// leap rules
const LEAP_FOUR_RULE: u64 = 4;
const LEAP_ONE_HUNDRED_RULE: u64 = 100;
const LEAP_FOUR_HUNDRED_RULE: u64 = 400;

// date to duration
const DAYS_IN_YEAR: u64 = 365;
const HOURS_IN_DAY: u64 = 24;
const MINUTES_IN_HOUR: u64 = 60;
const MINUTES_IN_DAY: u64 = MINUTES_IN_HOUR * HOURS_IN_DAY;
const JAN_MINUTES: u64 = MINUTES_IN_DAY * 31;
const FEB_MINUTES: u64 = MINUTES_IN_DAY * 28;
const MAR_MINUTES: u64 = MINUTES_IN_DAY * 31;
const APR_MINUTES: u64 = MINUTES_IN_DAY * 30;
const MAY_MINUTES: u64 = MINUTES_IN_DAY * 31;
const JUN_MINUTES: u64 = MINUTES_IN_DAY * 30;
const JUL_MINUTES: u64 = MINUTES_IN_DAY * 31;
const AUG_MINUTES: u64 = MINUTES_IN_DAY * 31;
const SEP_MINUTES: u64 = MINUTES_IN_DAY * 30;
const OCT_MINUTES: u64 = MINUTES_IN_DAY * 31;
const NOV_MINUTES: u64 = MINUTES_IN_DAY * 30;
const MINUTES_IN_YEAR: u64 = DAYS_IN_YEAR * MINUTES_IN_DAY;
const EPOCH_DAYS: u64 = (DATE_YEAR_MIN * DAYS_IN_YEAR) + (DATE_YEAR_MIN / LEAP_FOUR_RULE)
    - (DATE_YEAR_MIN / LEAP_ONE_HUNDRED_RULE)
    + (DATE_YEAR_MIN / LEAP_FOUR_HUNDRED_RULE);

// max min duration rules
const DURATION_YEAR_MAX: u64 = 1000000;
const DURATION_YEAR_MIN: u64 = 0;

const DURATION_DAY_MAX: u64 = 364;
const DURATION_DAY_MIN: u64 = 0;

const DURATION_HOUR_MAX: u64 = 23;
const DURATION_HOUR_MIN: u64 = 0;

const DURATION_MINUTE_MAX: u64 = 59;
const DURATION_MINUTE_MIN: u64 = 0;

// min max date rules
// epoch year
const DATE_YEAR_MIN: u64 = 1900;
// We devide by 1000 because roughly every 1000 years leads to an extra 2/3 years worth of leap days, which gives padding to the max duration
const DATE_YEAR_MAX: u64 = DATE_YEAR_MIN + DURATION_YEAR_MAX - (DURATION_YEAR_MAX / 1000);

const DATE_MONTH_MIN: u64 = 1;
const DATE_MONTH_MAX: u64 = 12;

const DATE_DAY_MIN: u64 = 1;
const DATE_DAY_MAX: [[u64; 12]; 2] = [
    [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
    [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
]; // 0 for non-leap years and 1 for leap years, which is convienient for being used with the is_leap_year function

const DATE_HOUR_MIN: u64 = 0;
const DATE_HOUR_MAX: u64 = 23;

const DATE_MINUTE_MIN: u64 = 0;
const DATE_MINUTE_MAX: u64 = 59;

// month nums
const FEB_MONTH: u64 = 2;
const MAR_MONTH: u64 = 3;
const APR_MONTH: u64 = 4;
const MAY_MONTH: u64 = 5;
const JUN_MONTH: u64 = 6;
const JUL_MONTH: u64 = 7;
const AUG_MONTH: u64 = 8;
const SEP_MONTH: u64 = 9;
const OCT_MONTH: u64 = 10;
const NOV_MONTH: u64 = 11;
const DEC_MONTH: u64 = 12;

// UTC Offset
const UTC_POSITIVE_SIGN: u64 = 0;
const UTC_NEGATIVE_SIGN: u64 = 1;
const UTC_HOUR_MAX: u64 = 23;
const UTC_HOUR_MIN: u64 = 0;
const UTC_MINUTE_MAX: u64 = 59;
const UTC_MINUTE_MIN: u64 = 0;

// formatting
const DATE_FORMATTING: usize = 3;
const DATE_MONTH_LOC: usize = 0;
const DATE_DAY_LOC: usize = 1;
const DATE_YEAR_LOC: usize = 2;
const TIME_FORMATTING: usize = 2;
const TIME_HOUR_LOC: usize = 0;
const TIME_MINUTE_LOC: usize = 1;

// Arg Lengths
const CALC_ARG_LEN: usize = 7;
const MISC_ARG_LEN: usize = 2;

// Arg Locations
const CALC_ARG_DATE1: usize = 1;
const CALC_ARG_TIME1: usize = 2;
const CALC_ARG_UTC1: usize = 3;
const CALC_ARG_DATE2: usize = 4;
const CALC_ARG_TIME2: usize = 5;
const CALC_ARG_UTC2: usize = 6;
const MISC_ARG_COMMAND: usize = 1;

// Duration Print Single Case Used for Printing
const DURATION_SINGLE: u64 = 1;

struct UtcOffset {
    pub sign: u64,
    pub hour: u64,
    pub minute: u64,
}

#[derive(Clone, Copy)]
struct DateTime {
    pub year: u64,
    pub month: u64,
    pub day: u64,
    pub hour: u64,
    pub minute: u64,
}

struct Duration {
    pub year: u64,
    pub day: u64,
    pub hour: u64,
    pub minute: u64,
}

fn is_leap_year(year: u64) -> bool {
    (year % LEAP_FOUR_RULE == 0 && year % LEAP_ONE_HUNDRED_RULE != 0)
        || (year % LEAP_FOUR_HUNDRED_RULE == 0)
}

fn check_date(date: &DateTime) {
    if date.year > DATE_YEAR_MAX {
        panic!(
            "Year is greater than the maximum year of {}.",
            DATE_YEAR_MAX
        );
    } else if date.year < DATE_YEAR_MIN {
        panic!("Year is less than the minimum year of {}.", DATE_YEAR_MIN);
    } else if date.month < DATE_MONTH_MIN {
        panic!(
            "Month is less than the minimum month of {}.",
            DATE_MONTH_MIN
        );
    } else if date.month > DATE_MONTH_MAX {
        panic!(
            "Month is greater than the maximum month of {}.",
            DATE_MONTH_MAX
        );
    } else if date.day < DATE_DAY_MIN {
        panic!("Day is less than the minimum day of {}.", DATE_DAY_MIN);
    } else if date.day > DATE_DAY_MAX[is_leap_year(date.year) as usize][date.month as usize - 1] {
        panic!(
            "Day is greater than the maximum day of {}.",
            DATE_DAY_MAX[is_leap_year(date.year) as usize][date.month as usize - 1]
        );
    } else if date.hour > DATE_HOUR_MAX {
        panic!(
            "Hour is greater than the maximum hour of {}.",
            DATE_HOUR_MAX
        );
    } else if date.minute > DATE_MINUTE_MAX {
        panic!(
            "Minute is greater than the maximum minute of {}.",
            DATE_MINUTE_MAX
        );
    }
}

fn check_utc_offset(utc_offset: &UtcOffset) {
    if utc_offset.sign != UTC_POSITIVE_SIGN && utc_offset.sign != UTC_NEGATIVE_SIGN {
        panic!("Improper sign for UTC Offset. Please contact developer.");
    } else if utc_offset.hour > UTC_HOUR_MAX {
        panic!(
            "UTC Offset hour is greater than the maximum hour of {}.",
            UTC_HOUR_MAX
        );
    } else if utc_offset.minute > UTC_MINUTE_MAX {
        panic!(
            "UTC Offset minute is greater than the maximum minute of {}.",
            UTC_MINUTE_MAX
        );
    }
}

fn check_duration(duration: &Duration) {
    if duration.year > DURATION_YEAR_MAX {
        panic!(
            "Year is greater than the maximum year of {}. Please contact developer.",
            DURATION_YEAR_MAX
        );
    } else if duration.day > DURATION_DAY_MAX {
        panic!(
            "Day is greater than the maximum day of {}. Please contact developer.",
            DURATION_DAY_MAX
        );
    } else if duration.hour > DURATION_HOUR_MAX {
        panic!(
            "Hour is greater than the maximum hour of {}. Please contact developer.",
            DURATION_HOUR_MAX
        );
    } else if duration.minute > DURATION_MINUTE_MAX {
        panic!(
            "Minute is greater than the maximum minute of {}. Please contact developer.",
            DURATION_MINUTE_MAX
        );
    }
}

fn date_from_strs(date: &str, time: &str) -> DateTime {
    let mut date_time: DateTime = DateTime {
        year: DATE_YEAR_MIN,
        month: DATE_MONTH_MIN,
        day: DATE_DAY_MIN,
        hour: DATE_HOUR_MIN,
        minute: DATE_MINUTE_MIN,
    };

    let date_strs: Vec<&str> = date.split('-').collect::<Vec<&str>>();
    let time_strs: Vec<&str> = time.split(':').collect::<Vec<&str>>();

    // check formating
    if date_strs.len() != DATE_FORMATTING {
        panic!("Date improperly formatted. Use \"mm-dd-yyyy\".");
    } else if time_strs.len() != TIME_FORMATTING {
        panic!("Time improperly formatted. Use \"hh:mm\"");
    }

    // parse month
    match date_strs[DATE_MONTH_LOC].parse::<u64>() {
        Ok(month) => date_time.month = month,
        Err(_e) => panic!("Unable to parse month."),
    }

    // parse day
    match date_strs[DATE_DAY_LOC].parse::<u64>() {
        Ok(day) => date_time.day = day,
        Err(_e) => panic!("Unable to parse day."),
    }

    // parse year
    match date_strs[DATE_YEAR_LOC].parse::<u64>() {
        Ok(year) => date_time.year = year,
        Err(_e) => panic!("Unable to parse year."),
    }

    // parse hour
    match time_strs[TIME_HOUR_LOC].parse::<u64>() {
        Ok(hour) => date_time.hour = hour,
        Err(_e) => panic!("Unable to parse hour."),
    }

    // parse minute
    match time_strs[TIME_MINUTE_LOC].parse::<u64>() {
        Ok(minute) => date_time.minute = minute,
        Err(_e) => panic!("Unable to parse minute."),
    }

    check_date(&date_time);

    date_time
}

fn utc_offset_from_str(mut utc_offset: &str) -> UtcOffset {
    let mut new_utc_offset: UtcOffset = UtcOffset {
        sign: UTC_POSITIVE_SIGN,
        hour: UTC_HOUR_MIN,
        minute: UTC_MINUTE_MIN,
    };

    // parse sign
    if !utc_offset.is_empty() {
        let sign = utc_offset.chars().next();
        if sign == Some('+') {
            new_utc_offset.sign = UTC_POSITIVE_SIGN;
        } else if sign == Some('-') {
            new_utc_offset.sign = UTC_NEGATIVE_SIGN;
        } else {
            panic!("Improper sign for UTC Offset. Please use either - or +.");
        }
        utc_offset = utc_offset.trim_start_matches(['+', '-']);
    }

    let utc_offset_strs: Vec<&str> = utc_offset.split(':').collect::<Vec<&str>>();

    // check formating
    if utc_offset_strs.len() != TIME_FORMATTING {
        panic!("UTC Offset improperly formatted. Use \"+ or -hh:mm\".");
    }

    // parse hour
    match utc_offset_strs[TIME_HOUR_LOC].parse::<u64>() {
        Ok(hour) => new_utc_offset.hour = hour,
        Err(_e) => panic!("Unable to parse hour."),
    }

    // parse minute
    match utc_offset_strs[TIME_MINUTE_LOC].parse::<u64>() {
        Ok(minute) => new_utc_offset.minute = minute,
        Err(_e) => panic!("Unable to parse minute."),
    }

    check_utc_offset(&new_utc_offset);

    new_utc_offset
}

// There has to be a more graceful way of doing this, but this should work for now
fn date_sub_utc_offset(date: &DateTime, utc_offset: &UtcOffset) -> DateTime {
    let mut new_date: DateTime = *date;

    // date - utc_offset
    if utc_offset.sign == UTC_POSITIVE_SIGN {
        // date minute - utc_offset minute
        // sub minute and no borrow
        if new_date.minute >= utc_offset.minute {
            new_date.minute -= utc_offset.minute;
        // sub minute and borrow
        } else {
            new_date.minute = (DATE_MINUTE_MAX + 1) - (utc_offset.minute - new_date.minute);

            // sub hour and no borrow
            if new_date.hour != DATE_HOUR_MIN {
                new_date.hour -= 1;
            // sub hour and borrow
            } else {
                new_date.hour = DATE_HOUR_MAX;

                // sub day and no borrow
                if new_date.day != DATE_DAY_MIN {
                    new_date.day -= 1;
                // sub day and borrow
                } else {
                    // must handle month and year first to get proper max day
                    // sub month and no borrow
                    if new_date.month != DATE_MONTH_MIN {
                        new_date.month -= 1;
                    // sub month and borrow
                    } else {
                        new_date.month = DATE_MONTH_MAX;

                        // sub year and no borrow
                        if new_date.year != DATE_YEAR_MIN {
                            new_date.year -= 1;
                        // panic for date going lower than epoch
                        } else {
                            panic!("UTC Offset causes date's year to go lower than the minimum year of {}.", DATE_YEAR_MIN);
                        }
                    }
                    // set the day to day max after handling month and year
                    new_date.day = DATE_DAY_MAX[is_leap_year(new_date.year) as usize]
                        [new_date.month as usize - 1];
                }
            }
        }

        // date hour - utc_offset hour
        // sub hour and no borrow
        if new_date.hour >= utc_offset.hour {
            new_date.hour -= utc_offset.hour;
        // sub hour and borrow
        } else {
            new_date.hour = (DATE_HOUR_MAX + 1) - (utc_offset.hour - new_date.hour);

            // sub day and no borrow
            if new_date.day != DATE_DAY_MIN {
                new_date.day -= 1;
            // sub day and borrow
            } else {
                // must handle month and year first to get proper max day
                // sub month and no borrow
                if new_date.month != DATE_MONTH_MIN {
                    new_date.month -= 1;
                // sub month and borrow
                } else {
                    new_date.month = DATE_MONTH_MAX;

                    // sub year and no borrow
                    if new_date.year != DATE_YEAR_MIN {
                        new_date.year -= 1;
                    // panic for date going lower than epoch
                    } else {
                        panic!("UTC Offset causes date's year to go lower than the minimum year of {}.", DATE_YEAR_MIN);
                    }
                }
                // set the day to day max after handling month and year
                new_date.day =
                    DATE_DAY_MAX[is_leap_year(new_date.year) as usize][new_date.month as usize - 1];
            }
        }
    // date + utc_offset
    } else if utc_offset.sign == UTC_NEGATIVE_SIGN {
        // date minute + utc_offset minute
        // add minute and no give
        if new_date.minute + utc_offset.minute <= DATE_MINUTE_MAX {
            new_date.minute += utc_offset.minute;
        // add minute and give
        } else {
            new_date.minute = (new_date.minute + utc_offset.minute) - (DATE_MINUTE_MAX + 1);

            // add hour and no give
            if new_date.hour != DATE_HOUR_MAX {
                new_date.hour += 1;
            // add hour and give
            } else {
                new_date.hour = DATE_HOUR_MIN;

                // add day and no give
                if new_date.day
                    != DATE_DAY_MAX[is_leap_year(new_date.year) as usize]
                        [new_date.month as usize - 1]
                {
                    new_date.day += 1;
                // add day and give
                } else {
                    new_date.day = DATE_DAY_MIN;

                    // add month and no give
                    if new_date.month != DATE_MONTH_MAX {
                        new_date.month += 1;
                    // add month and give
                    } else {
                        new_date.month = DATE_MONTH_MIN;

                        // add year and no give
                        if new_date.year != DATE_YEAR_MAX {
                            new_date.year += 1;
                        // panic for date going above max year
                        } else {
                            panic!("UTC Offset causes date's year to go greater than the maximum year of {}.", DATE_YEAR_MAX);
                        }
                    }
                }
            }
        }

        // date hour + utc_offset hour
        // add hour and no give
        if new_date.hour + utc_offset.hour <= DATE_HOUR_MAX {
            new_date.hour += utc_offset.hour;
        // add hour and give
        } else {
            new_date.hour = (new_date.hour + utc_offset.hour) - (DATE_HOUR_MAX + 1);

            // add day and no give
            if new_date.day
                != DATE_DAY_MAX[is_leap_year(new_date.year) as usize][new_date.month as usize - 1]
            {
                new_date.day += 1;
            // add day and give
            } else {
                new_date.day = DATE_DAY_MIN;

                // add month and no give
                if new_date.month != DATE_MONTH_MAX {
                    new_date.month += 1;
                // add month and give
                } else {
                    new_date.month = DATE_MONTH_MIN;

                    // add year and no give
                    if new_date.year != DATE_YEAR_MAX {
                        new_date.year += 1;
                    // panic for date going above max year
                    } else {
                        panic!("UTC Offset causes date's year to go greater than the maximum year of {}.", DATE_YEAR_MAX);
                    }
                }
            }
        }
    }

    new_date
}

fn date_to_minutes(date: &DateTime) -> u64 {
    // years to minutes
    // add 5 days of minutes as padding. This gives the freedom to have the Epoch year be a leap year
    let mut total_minutes = (((date.year * DAYS_IN_YEAR) + (date.year / LEAP_FOUR_RULE)
        - (date.year / LEAP_ONE_HUNDRED_RULE)
        + (date.year / LEAP_FOUR_HUNDRED_RULE)
        - EPOCH_DAYS)
        * MINUTES_IN_DAY)
        + (MINUTES_IN_DAY * 5);

    // days to minutes
    total_minutes += date.day * MINUTES_IN_DAY;
    // hours to minutes
    total_minutes += date.hour * MINUTES_IN_HOUR;
    // minutes to minutes
    total_minutes += date.minute;

    // months to minutes
    total_minutes += (date.month >= FEB_MONTH) as u64 * JAN_MINUTES; // add Jan minutes
    total_minutes += (date.month >= MAR_MONTH) as u64 * FEB_MINUTES; // add Feb minutes
    total_minutes += (date.month >= APR_MONTH) as u64 * MAR_MINUTES; // add Mar minutes
    total_minutes += (date.month >= MAY_MONTH) as u64 * APR_MINUTES; // add Apr minutes
    total_minutes += (date.month >= JUN_MONTH) as u64 * MAY_MINUTES; // add May minutes
    total_minutes += (date.month >= JUL_MONTH) as u64 * JUN_MINUTES; // add Jun minutes
    total_minutes += (date.month >= AUG_MONTH) as u64 * JUL_MINUTES; // add Jul minutes
    total_minutes += (date.month >= SEP_MONTH) as u64 * AUG_MINUTES; // add Aug minutes
    total_minutes += (date.month >= OCT_MONTH) as u64 * SEP_MINUTES; // add Sep minutes
    total_minutes += (date.month >= NOV_MONTH) as u64 * OCT_MINUTES; // add Oct minutes
    total_minutes += (date.month >= DEC_MONTH) as u64 * NOV_MINUTES; // add Nov minutes

    // account for leap years
    // This is due to the extra day in leap years already being accounted for in the years to minutes calculation
    // If it's not a leap year, everything is correct, but if it is a leap year, months before Mar need to subtract a day
    total_minutes -= (is_leap_year(date.year) && date.month < MAR_MONTH) as u64 * MINUTES_IN_DAY;

    total_minutes
}

fn minutes_to_duration(mut minutes: u64) -> Duration {
    let mut duration = Duration {
        year: DURATION_YEAR_MIN,
        day: DURATION_DAY_MIN,
        hour: DURATION_HOUR_MIN,
        minute: DURATION_MINUTE_MIN,
    };

    duration.year = minutes / MINUTES_IN_YEAR;
    minutes -= duration.year * MINUTES_IN_YEAR;

    duration.day = minutes / MINUTES_IN_DAY;
    minutes -= duration.day * MINUTES_IN_DAY;

    duration.hour = minutes / MINUTES_IN_HOUR;
    minutes -= duration.hour * MINUTES_IN_HOUR;

    duration.minute = minutes;

    check_duration(&duration);

    duration
}

fn print_duration(duration: &Duration) {
    print!("Duration: ");

    // print year
    if duration.year == DURATION_SINGLE {
        print!("{} year, ", duration.year);
    } else {
        print!("{} years, ", duration.year);
    }

    // print day
    if duration.day == DURATION_SINGLE {
        print!("{} day, ", duration.day);
    } else {
        print!("{} days, ", duration.day);
    }

    // print hour
    if duration.hour == DURATION_SINGLE {
        print!("{} hour, ", duration.hour);
    } else {
        print!("{} hours, ", duration.hour);
    }

    // print minute
    if duration.minute == DURATION_SINGLE {
        print!("{} minute", duration.minute);
    } else {
        print!("{} minutes", duration.minute);
    }

    // clear stdout
    println!();
}

fn display_version() {
    println!("TimeKeep - Fasting, Sobriety, Etc.");
    println!("Version 0.1");
    println!();
}

fn display_help() {
    println!("TimeKeep is a CLI that calculates the duration between two dates and times.");
    println!();
    println!("Example Format and Command:");
    println!("  -timekeep SD ST SUO ED ET EUO");
    println!("    -SD: Start Date");
    println!("    -ST: Start Time");
    println!("    -SUO: Start UTC Offset");
    println!("    -ED: End Date");
    println!("    -ET: End Time");
    println!("    -EUO: End UTC Offset");
    println!();
    println!("  -timekeep 03-12-2022 17:52 -05:00 10-22-2023 06:12 +07:00");
    println!();
    println!("-h, --help");
    println!("  -Provides name of the program, its version, a description of");
    println!("  -the program, list of flags, and examples.");
    println!();
    println!("-v, --version");
    println!("  -Provides the name of the program and its version.");
    println!();
    println!("Date Format:");
    println!("  -mm-dd-yyyy");
    println!("    -mm: Month between 1 and 12.");
    println!("    -dd: Day between 1 and 31. Must be valid.");
    println!(
        "    -yyyy: Year between {} and {}",
        DATE_YEAR_MIN, DATE_YEAR_MAX
    );
    println!();
    println!("Time Format:");
    println!("  -hh:mm");
    println!("    -hh: Hour between 0 and 23.");
    println!("    -mm: Minute between 0 and 59.");
    println!();
    println!("UTC Offset Format:");
    println!("  -[+ or -]hh:mm (e.g. +03:00 or -10:30)");
    println!("    -hh: Hour between 0 and 23.");
    println!("    -mm: Minute between 0 and 59.");
    println!("  -Must not cause the date's year to underflow or overflow.");
    println!();
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() == CALC_ARG_LEN {
        let mut date_time1 =
            date_from_strs(args[CALC_ARG_DATE1].as_str(), args[CALC_ARG_TIME1].as_str());
        let mut date_time2 =
            date_from_strs(args[CALC_ARG_DATE2].as_str(), args[CALC_ARG_TIME2].as_str());

        let utc_offset1 = utc_offset_from_str(args[CALC_ARG_UTC1].as_str());
        let utc_offset2 = utc_offset_from_str(args[CALC_ARG_UTC2].as_str());

        date_time1 = date_sub_utc_offset(&date_time1, &utc_offset1);
        date_time2 = date_sub_utc_offset(&date_time2, &utc_offset2);

        let minutes1 = date_to_minutes(&date_time1);
        let minutes2 = date_to_minutes(&date_time2);

        if minutes1 > minutes2 {
            panic!("The second date must be equal or greater than the first date.");
        }

        let duration = minutes_to_duration(minutes2 - minutes1);

        println!(
            "Date 1: {} {} {}",
            args[CALC_ARG_DATE1], args[CALC_ARG_TIME1], args[CALC_ARG_UTC1]
        );
        println!(
            "Date 2: {} {} {}",
            args[CALC_ARG_DATE2], args[CALC_ARG_TIME2], args[CALC_ARG_UTC2]
        );
        print_duration(&duration);
    } else if args.len() == MISC_ARG_LEN && args[MISC_ARG_COMMAND] == "version"
        || args.len() == MISC_ARG_LEN && args[MISC_ARG_COMMAND] == "-v"
    {
        display_version();
    // includes "help" and "-h" option
    } else {
        display_version();
        display_help();
    }
}
