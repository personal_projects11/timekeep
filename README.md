# Program Description
TimeKeep is a CLI that calculates the duration between two dates and times. This is useful for tracking fasting periods, sobriety, or anything that requires accounting for a duration between two dates and times. Changes in timezones and daylight savings times can be accounted for with the UTC offsets. If both dates and times will always have the same local times, then +00:00 can be used for the UTC Offsets.

## Example Commands
Format of commands
```
timekeep <start date> <start time> <start UTC offset> <end date> <end time> <end UTC offset>
```

Real example
```
timekeep 03-12-2022 17:52 -05:00 10-22-2023 06:12 +07:00
```

## Flags
```
-h, --help
```
Provides name of the program, its version, a description of the program, list of flags, and examples.

```
-v, --version
```
Provides the name of the program and its version.

## Date Format and Epoch
```
mm-dd-yyyy
```
Where mm is a value between 01 and 12, dd is a value between 01 and the max day of a valid month, and yyyy is a year greater than the programs min year, which is 1900, or greater than the programs max year, which is 1000900. The UTC Offset cannot cause the date and time go below the programs epoch or max year.

## Time Format
```
hh:mm
```
Where hh is a value between 00 and 23 and mm is a value between 00 and 59.

## UTC Offset Format
```
[+ or -]hh:mm (e.g. +03:00 or -10:30)
```
Where hh is the hour offset between 00 and 23 and mm is the minute offset between 00 and 59.